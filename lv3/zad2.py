import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 


mtcars = pd.read_csv('mtcars.csv')
##1 new_mtcars = mtcars.groupby('cyl').mpg.mean().plot.bar()

##1 plt.show()

#2 boxplot = mtcars.plot.box(by='cyl',column='wt')
#2 plt.show()

#3 fuel = mtcars.groupby('am').mpg.mean().plot.bar()
#3 plt.show()

#3.2 fuel = mtcars.plot.box(by='am',column='mpg')
#3.2 plt.show()
ax = mtcars[mtcars.am==0].plot.scatter(x='qsec',y='hp',color='Blue',label='0')
mtcars[mtcars.am==1].plot.scatter(x='qsec',y='hp',color='red',label='1',ax=ax)
plt.show()