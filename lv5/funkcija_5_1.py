from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt 
import pandas as pd
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.cluster import AgglomerativeClustering

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                    centers=4,
                                    cluster_std=[1.0, 2.5, 0.5, 3.0],
                                    random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X



data = generate_data(100, 1)
plt.figure(1)
i = 1
arrayInertia = []
while i !=21:
    kmeans = KMeans(n_clusters = i)
    kmeans.fit(data)
    y_kmeans = kmeans.predict(data)
    arrayInertia.append(kmeans.inertia_)
    i = i+1


print(arrayInertia)
i = 1
b = 0
while i!=21: 
    plt.scatter(x = i, y= arrayInertia[b])
    b = b+1
    i = i+1
#plt.show()

kmeans = KMeans(n_clusters = 3)
kmeans.fit(data)
y_kmeans = kmeans.predict(data)
plt.scatter(data[:,0], data[:,1], c = y_kmeans, s = 50, cmap = 'Accent')
centers = kmeans.cluster_centers_
plt.scatter(centers[:,0], centers[:,1], c = 'black', s = 200, alpha = 0.5)

data = generate_data(100, 3)
z = linkage(data, 'ward')
pic = plt.figure(figsize = (25,10))
dendrogram(z)
plt.show()