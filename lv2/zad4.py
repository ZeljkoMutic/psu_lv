import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
import skimage.io 
from skimage.transform import rotate

img = skimage.io.imread('tiger.png', as_gray=True)
for height in range(len(img)):
    for width in (len(img)):
        img[height][width] = img[width][height]

plt.figure(1)
plt.imshow(img,cmap = 'gray', vmin = 0, vmax = 255)
plt.show()